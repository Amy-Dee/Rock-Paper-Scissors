#!/usr/bin/env perl
use v5.12;
use open ':std', ':encoding(UTF-8)';
use utf8;
use Modern::Perl;
use String::Similarity;
use Readonly;
Readonly::Scalar our $VERSION => 1;

# Booleans.
use constant TRUE  => 1;
use constant FALSE => 0;
use constant ME    => 1;    # This corresponds to $i_won being TRUE.
use constant YOU   => 0;    # Not ME.

# Game vocabulary.
use constant MOVE           => 0;
use constant BEATS_ADJACENT => 1;
use constant BEATS_ACROSS   => 2;
Readonly::Array our @WORD   => (
	[qw{Rock     Paper     Scissors    Spock     Lizard  }],  # MOVE
	[qw{crushes  wraps     cuts        smashes   poisons }],  # BEATS_ADJACENT
	[qw{blunts   disproves decapitates vaporises eats    }],  # BEATS_ACROSS
);
Readonly::Scalar our $NUM_MOVES => @{ $WORD[MOVE] };

main();

sub main {
	print <<'WELCOME';
Welcome to the game of 
Rock-Paper-Scissors-Lizard-Spock, 
which is somewhat badly named because
that is not the order of precedence.
Blame The Big Bang Theory.

Type your move.
WELCOME
	my @score = ( 0, 0 );
	while (<>) {
		chomp;
		my $human = name_to_num($_);
		unless ( defined $human ) {
			say 'Quitting.';
			last;
		}
		my $computer = int rand $NUM_MOVES;
		printf(
			"You play %s, and I play %s.\n",
			$WORD[MOVE][$human],
			$WORD[MOVE][$computer],
		);

		# Have we again come to blows fruitlessly, noble foe?
		if ( $human == $computer ) {
			say 'That is a stalemate.';
			next;
		}

		use constant CLOSE => 1;
		use constant FAR   => 3;
		my @players = ( $computer, $human );

		my ( $i_won, $distance );
		foreach my $dist ( CLOSE, FAR ) {
			foreach my $alt ( keys @players ) {
				if ( $players[$alt]
					== ( ( $players[ $alt - 1 ] + $dist ) % $NUM_MOVES ) )
				{
					$i_won = ( $players[$alt] == $computer ) ? TRUE : FALSE;
					$distance = $dist;
				}
			}
		}

		# The aftermath.
		print $i_won ? 'Yay!' : 'Drat!';
		my ( $what_worked, $what_failed )
			= $i_won ? ( $computer, $human ) : ( $human, $computer );

		printf( " %s %s %s.\n",
			$WORD[MOVE][$what_worked],
			( $distance == CLOSE )
			? $WORD[BEATS_ADJACENT][$what_worked]
			: $WORD[BEATS_ACROSS][$what_worked],
			$WORD[MOVE][$what_failed] );

		$score[$i_won]++;
	}
	print "At the end of play, I have won $score[ME] rounds, "
		. "and you have won $score[YOU].\nWell done";
	say( ( $score[YOU] > $score[ME] ) ? q{.} : q{, me!} );
	exit $score[YOU] - $score[ME];
}

sub name_to_num {
	my $input = uc;
	Readonly my $THRESHOLD => 0.2;
	my ( $max, $best, $current, $option ) = 0;
	for ( 0 .. $NUM_MOVES - 1 ) {
		$option = uc $WORD[MOVE][$_];
		$current = similarity( $input, $option );
		if ( $current > $max ) {
			$max  = $current;
			$best = $_;
		}
	}
	$best = undef unless ( $max > $THRESHOLD );
	return $best;
}

__END__

=pod

=encoding utf8

=head1 NAME

Rock-Paper-Scissors-Lizard-Spock

=head1 SYNOPSIS

Run the script and follow the prompts
(entering ‘rock’, ‘paper’... etc, abbreviating if you like).

Type ‘quit’, or just the Enter key, or Ctrl-D to quit the game.

If you prefer, you can pass the game your
pre-determined list of moves from a file:

C<./rpsls.pl moves.txt>

Or via a pipe:

C<echo -e "rock \n rock \n scis \n pap \n sciss \n spock" | ./rpsls.pl>

=head1 DESCRIPTION

This is the classic game of
Rock-Paper-Scissors,
Paper-Scissors-Stone,
Zhot, or Roshambo... but extended, as per the show, The Big Bang Theory.

Play against the randomly-moving computer player.
At the end, your score is displayed.

=head1 RULES

Rock blunts Scissors and crushes Lizard.

Paper wraps Rock and disproves Spock.

Scissors cuts Paper and decapitates Lizard.

Lizard eats Paper and poisons Spock.

Spock smashes Scissors and vaporises Rock.


=head1 DEPENDENCIES

C<use Modern::Perl;>

C<use String::Similarity;>

C<use Readonly;>

=head1 EXIT STATUS

Returns the net number of rounds you have won
(negative if you have lost more than won).

=head1 AUTHOR

Amy Dee Dempster.

=head1 LICENCE AND COPYRIGHT

© 2018 Amy Dee Dempster.
For academic purposes only.

=cut

# Export this documentation for Bitbucket with:
# perl -CS -MPod::Markdown -e 'Pod::Markdown->new->filter(@ARGV)' rpsls.pl > README.md

