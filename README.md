# NAME

Rock-Paper-Scissors-Lizard-Spock

# SYNOPSIS

Run the script and follow the prompts
(entering ‘rock’, ‘paper’... etc, abbreviating if you like).

Type ‘quit’, or just the Enter key, or Ctrl-D to quit the game.

If you prefer, you can pass the game your
pre-determined list of moves from a file:

`./rpsls.pl moves.txt`

Or via a pipe:

`echo -e "rock \n rock \n scis \n pap \n sciss \n spock" | ./rpsls.pl`

# DESCRIPTION

This is the classic game of
Rock-Paper-Scissors,
Paper-Scissors-Stone,
Zhot, or Roshambo... but extended, as per the show, The Big Bang Theory.

Play against the randomly-moving computer player.
At the end, your score is displayed.

# RULES

Rock blunts Scissors and crushes Lizard.

Paper wraps Rock and disproves Spock.

Scissors cuts Paper and decapitates Lizard.

Lizard eats Paper and poisons Spock.

Spock smashes Scissors and vaporises Rock.

# DEPENDENCIES

`use Modern::Perl;`

`use String::Similarity;`

`use Readonly;`

# EXIT STATUS

Returns the net number of rounds you have won
(negative if you have lost more than won).

# AUTHOR

Amy Dee Dempster.

# LICENCE AND COPYRIGHT

© 2018 Amy Dee Dempster.
For academic purposes only.
